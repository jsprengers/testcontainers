# Testcontainers tutorial

A simple CRUD SpringBoot application backed by a MariaDB with two tables to illustrate usage of the Testcontainers framework.

This project shows the comparative advantage of using a prepared database image with persisted tables and data, compared to a blank factory image that is populated during the test setup.

In order to run all tests successfully, you must therefore first build that test image.

```bash
cd docker
docker build -t blockbusterdb .
# this will add the image to your local Docker repository
cd ..
mvn test
```
