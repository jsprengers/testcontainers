# This docker file lets you create a re-usable test image based on the standard mysql docker image with any number of create scripts.
# These scripts are executed during the build stage of the image, in contrast to the run stage, which is customary.
# This has the benefit that containers based on such an image will start up much more quickly, with all objects (tables, views, procedures) ready for use
# The only parts that you need to touch are the COPY lines in 15 and 16

FROM mariadb:10.7.1 as builder

# That file does the DB initialization but also runs mysql daemon, by removing the last line it will only init
RUN ["sed", "-i", "s/exec \"$@\"/echo \"not running $@\"/", "/usr/local/bin/docker-entrypoint.sh"]

# needed for intialization
ENV MARIADB_ROOT_PASSWORD=root
ENV MYSQL_DATABASE=blockbuster

COPY 1schema.sql /docker-entrypoint-initdb.d/
COPY 2data.sql /docker-entrypoint-initdb.d/

# Need to change the datadir to something else that /var/lib/mysql because the parent docker file defines it as a volume.
# https://docs.docker.com/engine/reference/builder/#volume :
#       Changing the volume from within the Dockerfile: If any build steps change the data within the volume after
#       it has been declared, those changes will be discarded.
RUN ["/usr/local/bin/docker-entrypoint.sh", "mysqld", "--datadir", "/initialized-db", "--aria-log-dir-path", "/initialized-db"]

FROM mariadb:10.7.1

ENV MARIADB_ROOT_PASSWORD=root

COPY --from=builder /initialized-db /var/lib/mysql
