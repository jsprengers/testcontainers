package nl.rockstars.testcontainers.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class Movie {
    long id;
    String title;
    String genre;
    LocalDate yearOfIssue;
}
