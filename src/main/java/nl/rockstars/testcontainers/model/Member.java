package nl.rockstars.testcontainers.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Value;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class Member {
    long id;
    String name;
    LocalDate dateOfBirth;
}
