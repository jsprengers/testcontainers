package nl.rockstars.testcontainers.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Value;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@Data
@AllArgsConstructor
@Builder(toBuilder = true)
public class Rental {

    public static final int MAX_LOAN_DAYS = 21;
    public static final float LATE_FINE_PER_DAY = 0.2f;

    long id;
    long memberId;
    long movieId;
    LocalDate dateBorrowed;
    LocalDate dateReturned;

    public static float calculateFine(LocalDate dateBorrowed, LocalDate dateReturned){
        long daysOnLoan = ChronoUnit.DAYS.between(dateBorrowed, dateReturned);
        return Math.max(0, daysOnLoan - MAX_LOAN_DAYS) * LATE_FINE_PER_DAY;
    }

}
