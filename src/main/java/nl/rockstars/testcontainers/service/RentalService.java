package nl.rockstars.testcontainers.service;

import nl.rockstars.testcontainers.model.Rental;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Map;
import java.util.Optional;

@Service
public class RentalService {

    @Autowired
    RentalDao rentalDao;

    public Rental createRental(long memberId, long movieId) {
        rentalDao.getMemberById(memberId).orElseThrow(() -> new IllegalStateException("No member with ID " + memberId));
        rentalDao.getMovieByID(movieId).orElseThrow(() -> new IllegalStateException("No movie with ID " + movieId));
        long openRentals = rentalDao.getOpenRentalsForMember(memberId).size();
        if (openRentals >= 3) {
            throw new IllegalStateException("Cannot rent more than three movies at the same time");
        }
        return rentalDao.createRental(memberId, movieId, LocalDate.now());
    }

    public Rental returnRental(long memberId, long movieId) {
        rentalDao.getMemberById(memberId).orElseThrow(() -> new IllegalStateException("No member with ID " + memberId));
        rentalDao.getMovieByID(movieId).orElseThrow(() -> new IllegalStateException("No movie with ID " + movieId));
        //check that movie is actually rented out
        Optional<Rental> rentalOpt =
                rentalDao.getOpenRentalsForMember(memberId).stream().filter(l -> l.getMovieId() == movieId).findFirst();
        return rentalOpt.map(rental -> {
                    LocalDate now = LocalDate.now();
                    rentalDao.returnRental(memberId, movieId, now);
                    return rental.toBuilder().dateReturned(now).build();
                })
                .orElseThrow(() -> new IllegalStateException("Member has no open rental for movie " + movieId));
    }
}
