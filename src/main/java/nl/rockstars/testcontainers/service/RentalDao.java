package nl.rockstars.testcontainers.service;

import nl.rockstars.testcontainers.model.Movie;
import nl.rockstars.testcontainers.model.Rental;
import nl.rockstars.testcontainers.model.Member;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface RentalDao {

    /**
     * @return entire contents of movie table
     */
    List<Movie> getAllMovies();

    /**
     * @return entire contents of member table
     */
    List<Member> getAllMembers();

    /**
     * @return contents rental table with date_borrowed in given range
     */
    List<Rental> getRentalsInPeriod(LocalDate from, LocalDate to);

    /**
     * Lookup a member by Id
     * @return Member object, or Optional.empty if no member is known
     */
    Optional<Member> getMemberById(long memberId);

    /**
     * Lookup a movie by Id
     * @return Movie object, or Optional.empty if no movie is known
     */
    Optional<Movie> getMovieByID(long id);

    /**
     * Returns all rentals for a member with a null return_date
     */
    List<Rental> getOpenRentalsForMember(long memberId);

    /**
     * Inserts a rental record for the member, movie and date. Does no further validation.
     */
    Rental createRental(long memberId, long movieId, LocalDate dateRented);

    /**
     * Updates the date_returned field in a given rental record
     */
    void returnRental(long memberId, long movieId, LocalDate dateReturned);


}
