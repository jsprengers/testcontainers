package nl.rockstars.testcontainers.service;

import nl.rockstars.testcontainers.model.Member;
import nl.rockstars.testcontainers.model.Movie;
import nl.rockstars.testcontainers.model.Rental;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class RentalDaoImpl implements RentalDao {

    @Autowired JdbcTemplate template;

    private static PreparedStatementSetter NO_PSTM_PARAMS = (PreparedStatement ps) -> {
    };

    public Optional<Member> getMemberById(long memberId) {
        return Optional.ofNullable(template.query("SELECT id,name,date_of_birth from member where id = ?",
                (ResultSet rs) -> entityFromResultSet(rs, this::memberFromResultSet), memberId));
    }

    public List<Member> getAllMembers() {
        return template.query("SELECT id,name,date_of_birth from member",
                NO_PSTM_PARAMS,
                (ResultSet rs, int rn) -> memberFromResultSet(rs));
    }

    private Member memberFromResultSet(ResultSet rs) {
        try {
            return new Member(rs.getLong("id"),
                    rs.getString("name"),
                    rs.getObject("date_of_birth", LocalDate.class));
        } catch (SQLException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

    public Optional<Movie> getMovieByID(long isbn) {
        return Optional.ofNullable(
                template.query("select id,title,genre,year_of_issue from movie where id = ?",
                        (ResultSet rs) -> entityFromResultSet(rs, this::movieFromResultSet), isbn));
    }

    public List<Movie> getAllMovies() {
        return template.query("SELECT id,title,genre,year_of_issue from movie",
                NO_PSTM_PARAMS,
                (ResultSet rs, int rn) -> movieFromResultSet(rs));
    }

    private Movie movieFromResultSet(ResultSet rs) {
        try {
            return new Movie(rs.getLong("id"),
                    rs.getString("title"),
                    rs.getString("genre"),
                    rs.getObject("year_of_issue", LocalDate.class));
        } catch (SQLException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

    @Override public List<Rental> getRentalsInPeriod(LocalDate from, LocalDate to) {
        return template.query("SELECT id,member_id,movie_id,date_borrowed,date_returned from rental where date_borrowed between ? and ?",
                pst -> {
                    pst.setObject(1, from);
                    pst.setObject(2, to);
                },
                (ResultSet rs, int rn) -> rentalFromResultSet(rs));
    }

    public List<Rental> getOpenRentalsForMember(long memberId) {
        return template.query(
                "select id,member_id,movie_id,date_borrowed,date_returned from rental where member_id = ? and date_returned is null",
                (ResultSet rs, int rn) -> rentalFromResultSet(rs), memberId);
    }

    private Rental rentalFromResultSet(ResultSet rs) {
        try {
            return new Rental(rs.getLong("id"),
                    rs.getLong("member_id"),
                    rs.getLong("movie_id"),
                    rs.getObject("date_borrowed", LocalDate.class),
                    rs.getObject("date_returned", LocalDate.class)
            );
        } catch (SQLException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

    @Override public Rental createRental(long memberId, long movieId, LocalDate now) {
        SimpleJdbcInsert insert = new SimpleJdbcInsert(template).withTableName("rental").usingGeneratedKeyColumns("id");
        long id =
                insert.executeAndReturnKey(Map.of("member_id", memberId, "movie_id", movieId, "date_borrowed", now)).longValue();
        return new Rental(id, memberId, movieId, now, null);
    }

    @Override public void returnRental(long memberId, long movieId, LocalDate dateReturned) {
        if (template.update("update rental set date_returned = ? where member_id = ? and movie_id = ?", dateReturned, memberId,
                movieId) != 1) {
            throw new IllegalStateException("Could not process rental return.");
        }
    }

    private <T> T entityFromResultSet(ResultSet rs, Function<ResultSet, T> factory) {
        try {
            return rs.next() ? factory.apply(rs) : null;
        } catch (SQLException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

}
