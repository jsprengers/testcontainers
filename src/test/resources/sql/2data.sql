USE blockbuster;
insert into movie (id, title, genre, year_of_issue)
values (1, 'First Blood', 'action', '1982-01-01')
     , (2, 'First Blood part 2', 'action', '1984-01-01')
     , (3, 'First Blood part 3', 'action', '1988-01-01')
     , (4, 'John Rambo', 'action', '2009-01-01')
     , (5, 'Rambo last blood', 'action', '2018-01-01')
     , (6, 'Eyes Wide Shut', 'drama', '1999-01-01')
     , (7, 'Full Metal Jacket', 'war', '1987-01-01')
     , (8, 'The Shining', 'thriller', '1980-01-01')
     , (9, 'Barry Lyndon', 'drama', '1975-01-01')
     , (10, 'A Clockwork Orange', 'drama', '1971-01-01')
     , (11, '2001: A Space Odyssey', 'scifi', '1968-01-01')
     , (12, 'Dr. Strangelove or: How I Learned to Stop Worrying and Love the Bomb', 'comedy', '1964-01-01')
     , (13, 'Lolita', 'drama', '1962-01-01')
     , (14, 'Spartacus', 'drama', '1960-01-01');

insert into member (id, name, date_of_birth)
values (1, 'Albert', '1970-03-10')
     , (2, 'Bert', '1975-09-03')
     , (3, 'Corrie', '1980-01-12')
     , (4, 'Daniel', '1985-07-05');

insert into rental (id, member_id, movie_id, date_borrowed, date_returned)
values (1, 1, 1, '2020-05-10', '2020-05-11')
     , (2, 1, 6, '2020-06-10', null);
