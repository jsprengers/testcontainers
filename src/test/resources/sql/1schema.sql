USE blockbuster;
CREATE TABLE movie
(
    id            BIGINT auto_increment primary key not null,
    title         varchar(250)                      not null,
    genre         varchar(50)                       not null,
    year_of_issue DATE                              not null
);

CREATE TABLE member
(
    id            BIGINT auto_increment primary key not null,
    name          varchar(250)                      not null,
    date_of_birth DATE                              not null
);

CREATE TABLE rental
(
    id            BIGINT auto_increment primary key not null,
    member_id     BIGINT                            not null,
    movie_id      BIGINT                            not null,
    date_borrowed DATE                              not null,
    date_returned DATE                              null,
    constraint member_loan foreign key (member_id) references member (id),
    constraint movie_id foreign key (movie_id) references movie (id),
    constraint unq_loan unique (member_id, movie_id, date_returned)
);
