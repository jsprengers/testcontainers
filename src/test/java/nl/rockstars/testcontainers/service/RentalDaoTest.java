package nl.rockstars.testcontainers.service;

import nl.rockstars.testcontainers.fixture.PrefabContainer;
import nl.rockstars.testcontainers.model.Member;
import nl.rockstars.testcontainers.model.Movie;
import nl.rockstars.testcontainers.model.Rental;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MariaDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * This test uses a prepared container which has tables and data already inserted, and is therefore notable faster than populating a factory image as part of the test setup.
 */
@SpringBootTest
@Testcontainers
class RentalDaoTest {

    @Container
    static MariaDBContainer<?> CONTAINER = new PrefabContainer();

    @DynamicPropertySource
    static void mysqlProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url",
                () -> String.format("jdbc:mysql://localhost:%d/blockbuster",
                        CONTAINER.getFirstMappedPort()));
    }

    @Autowired RentalDao rentalDao;

    @Test
    void getMemberById() {
        Member member = rentalDao.getMemberById(3L).orElseThrow();
        assertThat(member.getId()).isEqualTo(3L);
        assertThat(member.getName()).isEqualTo("Corrie");
        assertThat(member.getDateOfBirth()).isEqualTo(LocalDate.of(1980, 1, 12));
    }

    @Test
    void getUnknownMemberById() {
        assertThat(rentalDao.getMemberById(42L)).isNotPresent();
    }

    @Test
    void getAllMembers() {
        assertThat(rentalDao.getAllMembers()).extracting(Member::getId).containsExactlyInAnyOrder(1L, 2L, 3L, 4L);
    }

    @Test
    void getMovieByID() {
        Movie movie = rentalDao.getMovieByID(6).orElseThrow();
        assertThat(movie.getId()).isEqualTo(6L);
        assertThat(movie.getTitle()).isEqualTo("Eyes Wide Shut");
        assertThat(movie.getGenre()).isEqualTo("drama");
        assertThat(movie.getYearOfIssue()).isEqualTo(LocalDate.of(1999, 1, 1));
    }

    @Test
    void getUnknownMovieById() {
        assertThat(rentalDao.getMovieByID(42L)).isNotPresent();
    }

    @Test
    void getAllMovies() {
        assertThat(rentalDao.getAllMovies()).extracting(Movie::getId)
                .containsExactlyInAnyOrder(1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 9L, 10L, 11L, 12L, 13L, 14L);
    }

    @Test
    void getRentalsInPeriodHasOneRentals() {
        var rentals = rentalDao.getRentalsInPeriod(LocalDate.of(2020, 5, 9),
                LocalDate.of(2020, 5, 12));
        assertThat(rentals).hasSize(1);
        Rental rental = rentals.get(0);
        assertThat(rental.getId()).isEqualTo(1L);
        assertThat(rental.getMemberId()).isEqualTo(1L);
        assertThat(rental.getMovieId()).isEqualTo(1L);
        assertThat(rental.getDateBorrowed()).isEqualTo(LocalDate.of(2020,5,10));
        assertThat(rental.getDateReturned()).isEqualTo(LocalDate.of(2020,5,11));
    }

    @Test
    void getRentalsInPeriodHasNoRentals() {
        var rentals = rentalDao.getRentalsInPeriod(LocalDate.of(2021, 5, 10),
                LocalDate.of(2021, 6, 10));
        assertThat(rentals).isEmpty();
    }

    @Test
    void createRental() {
        Rental rental = rentalDao.createRental(2, 10, LocalDate.of(2021, 5, 1));
        assertThat(rental.getId()).isPositive();
        assertThat(rental.getMemberId()).isEqualTo(2);
        assertThat(rental.getMovieId()).isEqualTo(10);
        assertThat(rental.getDateBorrowed()).isEqualTo(LocalDate.of(2021, 5, 1));
    }

    @Test
    void returnRental() {
        var rental = rentalDao.createRental(2, 11, LocalDate.of(2021, 5, 8));
        rentalDao.returnRental(2, 11, LocalDate.of(2021, 5, 10));
        assertThat(rentalDao.getOpenRentalsForMember(11)).isEmpty();
    }


}
