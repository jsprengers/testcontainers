package nl.rockstars.testcontainers.service;

import nl.rockstars.testcontainers.model.Member;
import nl.rockstars.testcontainers.model.Movie;
import nl.rockstars.testcontainers.model.Rental;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;

@ExtendWith(MockitoExtension.class)
class RentalServiceTest {

    @Mock(lenient = true)
    RentalDao rentalDao;
    Rental rental = Rental.builder().build();
    @Mock
    Movie movie;
    @Mock
    Member member;

    @InjectMocks
    RentalService rentalService;

    @BeforeEach
    void setup() {
        doReturn(Optional.of(movie)).when(rentalDao).getMovieByID(2L);
        doReturn(Optional.of(member)).when(rentalDao).getMemberById(1L);
    }

    @Test
    void testCreateRental() {
        doReturn(rental).when(rentalDao).createRental(eq(1L), eq(2L), any(LocalDate.class));
        Rental created = rentalService.createRental(1L, 2);
        assertThat(created).isSameAs(rental);
    }

    @Test
    void testReturnRental() {
        rental.setMovieId(2L);
        doReturn(List.of(rental)).when(rentalDao).getOpenRentalsForMember(1L);
        Rental created = rentalService.returnRental(1L, 2);
        assertThat(created.getDateReturned()).isToday();
    }

    @Test
    void cannot_rent_more_than_three_movies_at_the_same_time() {
        doReturn(List.of(rental, rental, rental)).when(rentalDao).getOpenRentalsForMember(1L);
        assertThatThrownBy(() -> rentalService.createRental(1L, 2L)).hasMessage("Cannot rent more than three movies at the same time");
    }

    @Test
    void cannot_return_rental_that_is_not_rented_out_by_member() {
        assertThatThrownBy(() -> rentalService.returnRental(1L, 2L)).hasMessage("Member has no open rental for movie 2");
    }

    @Test
    void cannot_rent_unknown_movie() {
        assertThatThrownBy(() -> rentalService.createRental(1L, 20L)).hasMessage("No movie with ID 20");
    }

    @Test
    void cannot_rent_for_unknown_member() {
        assertThatThrownBy(() -> rentalService.createRental(2L, 2L)).hasMessage("No member with ID 2");
    }

    @Test
    void cannot_return_rental_for_unknown_movie() {
        assertThatThrownBy(() -> rentalService.returnRental(1L, 20L)).hasMessage("No movie with ID 20");
    }

    @Test
    void cannot_return_rental_for_unknown_member() {
        assertThatThrownBy(() -> rentalService.returnRental(2L, 2L)).hasMessage("No member with ID 2");
    }

}
