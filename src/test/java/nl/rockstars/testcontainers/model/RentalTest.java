package nl.rockstars.testcontainers.model;

import org.assertj.core.data.Offset;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

class RentalTest {

    private static final LocalDate oct28 = LocalDate.of(2021, 10, 28);
    private static final LocalDate oct7 = LocalDate.of(2021, 10, 7);
    private static final LocalDate sept20 = LocalDate.of(2021, 9, 28);

    @Test
    public void _21_day_loan_no_fine(){
        assertEquals(0, Rental.calculateFine(oct7, oct28));
    }

    @Test
    public void _nine_days_overdue_has_1_8_fine(){
        assertThat(Rental.calculateFine(sept20, oct28)).isEqualTo(1.8f, Offset.offset(0.001f));
    }

}
