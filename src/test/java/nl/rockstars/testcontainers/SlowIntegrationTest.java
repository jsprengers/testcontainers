package nl.rockstars.testcontainers;

import nl.rockstars.testcontainers.fixture.EmptyContainer;
import nl.rockstars.testcontainers.fixture.PrefabContainer;
import org.assertj.core.api.Fail;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.MariaDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * This test creates a container using a blank factory image and populating it with files in src/test/resources/sql.
 * This is notably slower than the RentalDaoTest, which uses a prepared image.
 */
@Testcontainers
class SlowIntegrationTest {

    @Container
    static MariaDBContainer<?> CONTAINER = new EmptyContainer();

    @Test
    void schemaShouldContain14Movies() {
        assertThat(getCountFromTableById("select count(1) from movie")).isEqualTo(14);
    }

    @Test
    void schemaShouldContain5EightiesMovies() {
        assertThat(getCountFromTableById("select count(1) from movie where year_of_issue between '1980-01-01' and '1989-12-31'")).isEqualTo(
                5);
    }

    @Test
    void schemaShouldContain5DramaMovies() {
        assertThat(getCountFromTableById("select count(1) from movie where genre = 'drama'")).isEqualTo(5);
    }

    @Test
    void schemaShouldContain4Members() {
        assertThat(getCountFromTableById("select count(1) from member")).isEqualTo(4);
    }

    @Test
    void schemaShouldContain2Rentals() {
        assertThat(getCountFromTableById("select count(1) from rental")).isEqualTo(2);
    }

    private int getCountFromTableById(String sql) {
        try (Connection connection = newConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            return resultSet.next() ? resultSet.getInt(1) : 0;
        } catch (SQLException e) {
            throw new IllegalStateException(e.getMessage());
        }
    }

    private Connection newConnection(){
        try {
            return CONTAINER.createConnection("");
        } catch (SQLException e) {
            throw new IllegalStateException("Could not create connection "+e.getMessage());
        }
    }

}
