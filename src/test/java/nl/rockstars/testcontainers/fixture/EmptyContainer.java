package nl.rockstars.testcontainers.fixture;

import org.testcontainers.containers.MariaDBContainer;
import org.testcontainers.utility.DockerImageName;

import static org.testcontainers.containers.BindMode.READ_ONLY;

public class EmptyContainer extends MariaDBContainer {
    public static final String ENTRYPOINT = "/docker-entrypoint-initdb.d/";
    public EmptyContainer() {
        super(DockerImageName.parse("mariadb:10.7.1"));
        withDatabaseName("blockbuster")
                .withEnv("MARIADB_ROOT_PASSWORD", "root")
                .withClasspathResourceMapping("sql/1schema.sql", ENTRYPOINT, READ_ONLY)
                .withClasspathResourceMapping("sql/2data.sql", ENTRYPOINT, READ_ONLY);
    }
}
