package nl.rockstars.testcontainers.fixture;

import org.testcontainers.containers.MariaDBContainer;
import org.testcontainers.utility.DockerImageName;

public class PrefabContainer extends MariaDBContainer {

    public PrefabContainer() {
        super(DockerImageName.parse("blockbusterdb:latest").asCompatibleSubstituteFor("mariadb"));
        withDatabaseName("blockbuster")
                .withUsername("root")
                .withPassword("root");
    }
}
